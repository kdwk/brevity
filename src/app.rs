use relm4::{
    actions::{RelmAction, RelmActionGroup},
    adw,
    component::{AsyncComponent, AsyncComponentParts, SimpleAsyncComponent},
    gtk, main_application, AsyncComponentSender, Component, ComponentController, ComponentParts,
    ComponentSender, Controller, SimpleComponent,
};

use gtk::prelude::*;
use gtk::{gio, glib};

use ashpd::desktop::file_chooser::{self, SelectedFiles};

use crate::modals::about::AboutDialog;
use crate::{
    config::{APP_ID, PROFILE},
    whoops::NoneError,
};
use crate::{
    document::{
        with, Create, Document, FileSystemEntity,
        Folder::{self, Project, User},
        LinesBufReaderFileExt, Map, Mode,
        Project::{Config, Data},
        ResultDocumentBoxErrorExt,
        User::{Documents, Downloads, Pictures},
    },
    whoops::{attempt, Catch, IntoWhoops, Whoops},
};
use tracker::track;

#[track]
pub(super) struct App {
    // about_dialog: AboutDialog,
    text_view: gtk::TextView,
    buffer: gtk::TextBuffer,
    message: Option<String>,
}

#[derive(Debug)]
pub(super) enum AppInput {
    Undo,
    Redo,
    Bold,
    AddImage,
    Quit,
}

relm4::new_action_group!(pub(super) WindowActionGroup, "win");
relm4::new_stateless_action!(PreferencesAction, WindowActionGroup, "preferences");
relm4::new_stateless_action!(pub(super) ShortcutsAction, WindowActionGroup, "show-help-overlay");
relm4::new_stateless_action!(AboutAction, WindowActionGroup, "about");

#[relm4::component(pub async)]
impl AsyncComponent for App {
    type Init = ();
    type Input = AppInput;
    type Output = ();
    type CommandOutput = ();

    menu! {
        primary_menu: {
            section! {
                "_Preferences" => PreferencesAction,
                "_Keyboard" => ShortcutsAction,
                "_About Brevity" => AboutAction,
            }
        }
    }

    view! {
        main_window = adw::ApplicationWindow::new(&main_application()) {
            set_visible: true,

            connect_close_request[sender] => move |_| {
                sender.input(AppInput::Quit);
                glib::Propagation::Stop
            },

            #[wrap(Some)]
            set_help_overlay: shortcuts = &gtk::Builder::from_resource(
                    "/app/drey/Brevity/gtk/help-overlay.ui"
                )
                .object::<gtk::ShortcutsWindow>("help_overlay")
                .unwrap() -> gtk::ShortcutsWindow {
                    set_transient_for: Some(&main_window),
                    set_application: Some(&main_application()),
            },

            add_css_class?: if PROFILE == "Devel" {
                    Some("devel")
                } else {
                    None
                },

            gtk::Box {
                set_orientation: gtk::Orientation::Vertical,

                adw::HeaderBar {
                    pack_start = &gtk::Box {
                        set_orientation: gtk::Orientation::Horizontal,

                        gtk::Button {
                            set_icon_name: "arrow-hook-left-horizontal",
                            set_tooltip_text: Some("_Undo"),
                            connect_clicked => AppInput::Undo,
                        },
                        gtk::Button {
                            set_icon_name: "arrow-hook-right-horizontal",
                            set_tooltip_text: Some("_Redo"),
                            connect_clicked => AppInput::Redo,
                        },
                        gtk::Button {
                            set_icon_name: "text-bold",
                            set_tooltip_text: Some("_Bold"),
                            connect_clicked => AppInput::Bold,
                        },
                        gtk::Button {
                            set_icon_name: "image-alt",
                            set_tooltip_text: Some("_Add Image"),
                            connect_clicked => AppInput::AddImage,
                        }
                    },
                    pack_end = &gtk::MenuButton {
                        set_icon_name: "open-menu-symbolic",
                        set_menu_model: Some(&primary_menu),
                    }
                },

                adw::ToastOverlay {
                    #[track = "model.changed(App::message())"]
                    optionally_add_toast: model.message.clone(),
                    gtk::ScrolledWindow {
                        model.text_view.clone() {
                            set_vexpand: true,
                        },
                    }
                }
            }

        }
    }

    async fn init(
        init: Self::Init,
        root: Self::Root,
        sender: AsyncComponentSender<Self>,
    ) -> AsyncComponentParts<Self> {
        // let about_dialog = AboutDialog::builder()
        // .transient_for(&root)
        // .launch(())
        // .detach();

        let buffer = gtk::TextBuffer::new(Some(&gtk::TextTagTable::new()));

        let model = Self {
            // about_dialog,
            text_view: gtk::TextView::with_buffer(&buffer.clone()),
            buffer: buffer,
            message: None,
            tracker: 0,
        };

        model.load().await;

        let widgets = view_output!();

        let mut actions = RelmActionGroup::<WindowActionGroup>::new();

        let shortcuts_action = {
            let shortcuts = widgets.shortcuts.clone();
            RelmAction::<ShortcutsAction>::new_stateless(move |_| {
                shortcuts.present();
            })
        };

        let about_action = {
            // let sender = model.about_dialog.sender().clone();
            RelmAction::<AboutAction>::new_stateless(move |_| {
                // sender.send(()).unwrap();
            })
        };

        actions.add_action(shortcuts_action);
        actions.add_action(about_action);
        actions.register_for_widget(&widgets.main_window);

        widgets.load_window_size();

        AsyncComponentParts { model, widgets }
    }

    async fn update(
        &mut self,
        message: Self::Input,
        _sender: AsyncComponentSender<Self>,
        _root: &Self::Root,
    ) {
        self.reset();
        match message {
            AppInput::Undo => self.buffer.undo(),
            AppInput::Redo => self.buffer.redo(),
            AppInput::Bold => {
                if let Some((iter_start, iter_end)) = self.buffer.selection_bounds() {}
            }
            AppInput::AddImage => {
                async fn prompt(text_view: gtk::TextView, buffer: gtk::TextBuffer) -> Whoops {
                    let file = SelectedFiles::open_file()
                        .title("Insert file")
                        .accept_label("Select")
                        .modal(true)
                        .multiple(false)
                        .send()
                        .await?
                        .response()?;
                    let image = Document::at(
                        Project(Data(&["UserImages"]).with_id("app", "drey", "Brevity")),
                        "image",
                        Create::AutoRenameIfExists,
                    )?;
                    std::fs::copy(
                        match gio::File::for_uri(file.uris()[0].as_str()).path() {
                            Some(path) => path,
                            None => Err(NoneError)?,
                        },
                        image.path().clone(),
                    );
                    with(&[Ok(image)], |d| {
                        let image = gtk::Image::builder()
                            .file(d["image"].path())
                            .height_request(300)
                            .width_request(300)
                            .build();
                        let anchor = buffer.create_child_anchor(&mut buffer.end_iter());
                        text_view.add_child_at_anchor(&image, &anchor);
                        Ok(())
                    });
                    Ok(())
                }
                _ = prompt(self.text_view.clone(), self.buffer.clone()).await;
            }
            AppInput::Quit => {
                self.save().await;
                main_application().quit();
            }
        }
    }

    fn shutdown(&mut self, widgets: &mut Self::Widgets, _output: relm4::Sender<Self::Output>) {
        widgets.save_window_size().unwrap();
    }
}

impl App {
    async fn save(&self) -> Result<(), glib::BoolError> {
        let settings = gio::Settings::new(APP_ID);
        let buffer_content = self.buffer.slice(
            &self.buffer.iter_at_offset(0),
            &self.buffer.end_iter(),
            true,
        );
        settings.set_string("buffer-content", &buffer_content)?;

        Ok(())
    }
    async fn load(&self) -> Result<(), glib::BoolError> {
        let settings = gio::Settings::new(APP_ID);
        let buffer_content = settings.string("buffer-content");
        self.buffer.set_text(&buffer_content);

        Ok(())
    }
}

impl AppWidgets {
    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let settings = gio::Settings::new(APP_ID);
        let (width, height) = self.main_window.default_size();

        settings.set_int("window-width", width)?;
        settings.set_int("window-height", height)?;

        settings.set_boolean("is-maximized", self.main_window.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let settings = gio::Settings::new(APP_ID);

        let width = settings.int("window-width");
        let height = settings.int("window-height");
        let is_maximized = settings.boolean("is-maximized");

        self.main_window.set_default_size(width, height);

        if is_maximized {
            self.main_window.maximize();
        }
    }
}

trait OptionallyAddToast {
    fn optionally_add_toast(&self, message: Option<String>);
}

impl OptionallyAddToast for adw::ToastOverlay {
    fn optionally_add_toast(&self, message: Option<String>) {
        if let Some(message) = message {
            self.add_toast(adw::Toast::new(&message));
        }
    }
}
